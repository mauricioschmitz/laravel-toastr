@if(Session::has('toastr.alerts'))
    <div id="toastr"></div>
    <script>
        if(typeof Vue !== 'undefined'){
            $('#toastr').append('<vue-toastr ref="toastr"></vue-toastr>');
            new Vue({
                el: '#toastr',
                mounted(){
                    @foreach(Session::get('toastr.alerts') as $alert)
                        this.$root.$refs.toastr.Add({
                        {!! (!empty($alert['title']) ? 'title: "'.$alert['title'].'", // Toast Title': '') !!}
                        msg: "{{ $alert['message'] }}", // Message
                        clickClose: true, // Click Close Disable
                        timeout: 4000, // Remember defaultTimeout is 5 sec..
                        progressBarValue: 0, // Manually update progress bar value later; null (not 0) is default
                        position: "toast-top-right", // Toast Position.
                        type: "{{ $alert['type'] }}" // Toast type
                    });
                    @endforeach
                }
            });
        }else{
            $(document).ready(function() {
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        timeOut: 4000
                    };

                    @foreach(Session::get('toastr.alerts') as $alert)
                    toastr.{{ $alert['type'] }}('{{ $alert['message'] }}' @if( ! empty($alert['title'])), '{{ $alert['title'] }}' @endif);
                    @endforeach
                }, 1300);
            });
        }
    </script>
@endif