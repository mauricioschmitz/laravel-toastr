<?php 

namespace Laraveltoastr\Toastr\Facades;

use Illuminate\Support\Facades\Facade;

class Toastr extends Facade {

    public static function getFacadeAccessor()
    {
        return 'Laraveltoastr\Toastr\Toastr';
    }

} 